a="Pikachu"
print(a[-2])
print(a[0])
#string slicing
#for first four strings
print(a[0:4])
print(a[-5:-1])

#more about strings
print(a[:])
print(a[:5])
print(a[2:])
print(len(a))

l=len(a)
print(l)


#string operators

print("Pika"+"Pika")
a='a'
b='b'
c='c'
print(a+b+c)
print(a*5)