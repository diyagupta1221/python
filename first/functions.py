def Add(a,b):
    print(a+b)
Add(2,3)

def food():
    print("CHICKEN BURGERS ARE FOR EVERYONE")
food()
print("functions can be called as many times as you want ")

food()
food()
food()

#more about functions
def Order(item):
    if(item=="CHICKEN BURGER"):
        return 1
    elif(item=="FRENCH FRIES"):
        return 2
itemname= input("Enter the item name")
result=Order(itemname)
print("result is",result)
if(result==1):
    print("5$,Thank You for shopping")
elif(result==2):
    print("7$,Thank You for shopping")

#default argument
def price(qty=1):
    print("Price : ",qty*40, "$")
price()