a=[1,2,3,4,5]
print(a)

colors=["red","yellow","green"]
print(colors)

print(colors[0])
print(colors[:])
print(colors[1:3])

#making an empty list
empty=[]
print(empty)
colors=colors+["white","orange"]
print(colors)

#list with conditional statements
list=["larry","curly","moe"]
if "curly" in list:
    print("yay,curly is in the list")

#list methods or list functions
#list.append()
#insert,remove,sort,reverse

partymem=[]
partymem.append("Castle")
print(partymem)

partymem.append("House")
print(partymem)

partymem.insert(0,"Patrick")
print(partymem)

partymem.remove("House")
print(partymem)

list2=[1,4,2,3]
list2.sort()
print(list2)

list2.reverse()
print(list2)